(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/tab1/tab1.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/tab1/tab1.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-segment [(ngModel)]=\"accion\">\n    <ion-grid>\n      <ion-row>\n        <div class=\"ion-float-left\">\n          <img src=\"/assets/img/logo.png\" height=\"40\" width=\"40\" alt=\"\" class=\"logo\"/>      \n        </div>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"6\">\n          <ion-segment-button value=\"guia\">\n            <ion-icon name=\"flash\"></ion-icon>\n            <ion-label>Guia turistica</ion-label>\n          </ion-segment-button>\n        </ion-col>\n        <ion-col size=\"6\">\n          <ion-segment-button value=\"taxi\">\n            <ion-icon name=\"flash\"></ion-icon>\n            <ion-label>Taxi</ion-label>\n          </ion-segment-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-segment>\n</ion-header>\n\n<ion-content class=\"background\">\n\n  <div *ngIf=\"accion == 'guia'\">\n      <ion-card class=\"card\">\n        <ion-card-header class=\"card-header\">\n          <div class=\"ion-float-left\">\n            Alameda central\n          </div>\n          <div class=\"ion-float-right\">\n            500$\n          </div>\n        </ion-card-header>\n        <ion-card-content class=\"ion-margin-top\">\n          <p>Atracción turistica . Av Mecixo 5843 museo de arte en una hacienda histórica abierta hasta 6:00pm</p>\n          <div>\n            <ion-grid>\n              <ion-row>\n                <ion-col size=\"7\">\n                  <ion-button fill=\"clear\" class=\"btn-trip\">Programar viaje</ion-button>\n                </ion-col>\n                <ion-col size=\"5\">\n                  <ion-button fill=\"clear\" class=\"btn-trip\">tomar viaje</ion-button>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </div>\n        </ion-card-content>\n    </ion-card>\n  </div>\n\n  <div *ngIf=\"accion == 'taxi'\">\n    <ion-card class=\"card\">\n      <ion-card-header class=\"card-header-turist\">\n        <div class=\"ion-float-left\">\n          Alameda central\n        </div>\n        <div class=\"ion-float-right\">\n          Viaje turistico\n        </div>\n      </ion-card-header>\n      <ion-card-content class=\"ion-margin-top\">\n        <p>Atracción turistica . Av Mecixo 5843 museo de arte en una hacienda histórica abierta hasta 6:00pm</p>\n        <div>\n          <ion-grid>\n            <ion-row>\n              <ion-col size=\"2\"></ion-col>\n              <ion-col size=\"5\">\n                <ion-button fill=\"clear\" class=\"btn-trip\">Tomar viaje</ion-button>\n              </ion-col>\n              <ion-col size=\"5\">\n                <ion-button fill=\"clear\" class=\"btn-trip\">Ver detalle</ion-button>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </div>\n      </ion-card-content>\n    </ion-card>\n  </div>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/tab1/tab1.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/tab1/tab1.module.ts ***!
  \*******************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab1.page */ "./src/app/pages/tab1/tab1.page.ts");







let Tab1PageModule = class Tab1PageModule {
};
Tab1PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"] }])
        ],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"]]
    })
], Tab1PageModule);



/***/ }),

/***/ "./src/app/pages/tab1/tab1.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/tab1/tab1.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".background {\n  --background: url(\"/assets/img/bg.png\") 0 0/100% 100% no-repeat;\n}\n\n.box-gradient {\n  margin: 0;\n  height: 100%;\n  border-left: 2px solid #a9da1f;\n  border-right: 2px solid #01acca;\n  background-size: 100% 2px;\n}\n\n.card {\n  --background: #f2f2f2 ;\n}\n\n.card-header {\n  border-bottom: 1px solid #cecece;\n  height: 40px;\n  font-size: 15px;\n  font-weight: 700;\n}\n\n.segment-button-checked {\n  background: -webkit-gradient(linear, left top, right top, from(#02b9c1), to(#83da22)) !important;\n  background: linear-gradient(90deg, #02b9c1, #83da22) !important;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2pvc2UvUHJvamVjdHMvdGF4aS9wYXNhamVyby10YXgvc3JjL2FwcC9wYWdlcy90YWIxL3RhYjEucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy90YWIxL3RhYjEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsK0RBQUE7QUNDRjs7QURFQTtFQUNFLFNBQUE7RUFDQSxZQUFBO0VBQ0EsOEJBQUE7RUFDQSwrQkFBQTtFQUNBLHlCQUFBO0FDQ0Y7O0FERUE7RUFDRSxzQkFBQTtBQ0NGOztBREVBO0VBQ0UsZ0NBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDQ0Y7O0FERUE7RUFFRSxnR0FBQTtFQUFBLCtEQUFBO0VBQ0EsWUFBQTtBQ0FGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGFiMS90YWIxLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iYWNrZ3JvdW5ke1xuICAtLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1nL2JnLnBuZ1wiKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcbn1cblxuLmJveC1ncmFkaWVudHtcbiAgbWFyZ2luOiAwO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJvcmRlci1sZWZ0OiAycHggc29saWQgI2E5ZGExZjtcbiAgYm9yZGVyLXJpZ2h0OiAycHggc29saWQgIzAxYWNjYTtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlIDJweDtcbn1cblxuLmNhcmR7XG4gIC0tYmFja2dyb3VuZDogI2YyZjJmMlxufVxuXG4uY2FyZC1oZWFkZXJ7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2VjZWNlO1xuICBoZWlnaHQ6IDQwcHg7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuLnNlZ21lbnQtYnV0dG9uLWNoZWNrZWR7XG4gIC8vIC0tYmFja2dyb3VuZDogO1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoOTBkZWcsICMwMmI5YzEsICM4M2RhMjIpICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cbiIsIi5iYWNrZ3JvdW5kIHtcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZy9iZy5wbmdcIikgMCAwLzEwMCUgMTAwJSBuby1yZXBlYXQ7XG59XG5cbi5ib3gtZ3JhZGllbnQge1xuICBtYXJnaW46IDA7XG4gIGhlaWdodDogMTAwJTtcbiAgYm9yZGVyLWxlZnQ6IDJweCBzb2xpZCAjYTlkYTFmO1xuICBib3JkZXItcmlnaHQ6IDJweCBzb2xpZCAjMDFhY2NhO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCUgMnB4O1xufVxuXG4uY2FyZCB7XG4gIC0tYmFja2dyb3VuZDogI2YyZjJmMiA7XG59XG5cbi5jYXJkLWhlYWRlciB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2VjZWNlO1xuICBoZWlnaHQ6IDQwcHg7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IDcwMDtcbn1cblxuLnNlZ21lbnQtYnV0dG9uLWNoZWNrZWQge1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoOTBkZWcsICMwMmI5YzEsICM4M2RhMjIpICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiB3aGl0ZTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/tab1/tab1.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/tab1/tab1.page.ts ***!
  \*****************************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let Tab1Page = class Tab1Page {
    constructor() {
        this.accion = 'guia';
    }
};
Tab1Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab1',
        template: __webpack_require__(/*! raw-loader!./tab1.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/tab1/tab1.page.html"),
        styles: [__webpack_require__(/*! ./tab1.page.scss */ "./src/app/pages/tab1/tab1.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], Tab1Page);



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module-es2015.js.map