import { Component } from '@angular/core';
import { CorekService } from '../../services/corek.service'
import * as moment from 'moment';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  accion = 'guia'
  trips: any[] = [];
  loader:boolean = true;
  constructor(
    private _corek:CorekService
  ) {}

  ionViewWillEnter(){
    let conection = Date.now().toString+'conection'+Math.random();
    this._corek.ConnectCorekconfig(conection);
    this._corek.socket.on(conection, (response)=>{
      this.searchTrips();
    });
  }

  searchTrips(){
    this.trips = [];
    let queryTrips = Date.now().toString+'queryPost'+Math.random();
    this._corek.socket.emit('query_post', { 'condition': {"post_status":1}, 'event':queryTrips});
    this._corek.socket.on(queryTrips, (trips) => {
      let locations = ' '; 
      if(trips.length > 0){
        for (let trip of trips){
          let directions = JSON.parse(trip.post_content)
          for (let direction of directions){
            locations += direction.name.substr(0,40)+' - ';
          }
          let dateTime = moment(trip.post_date_gmt).add(4, 'hours').format("HH:mm");
          this.trips.push({id:trip.ID ,title:trip.post_title, type:trip.post_type, point:locations, time:dateTime})
        }
        this.loader = false;
      }else{

      }
    });
  }

}
